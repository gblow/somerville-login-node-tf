# Configure the OpenStack Provider
terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
}

provider "openstack" {
  cloud  = "openstack-sandbox" # cloud defined in cloud.yml file
  max_retries = 5
}

# Variables
variable "keypair" {
  type    = string
  default = "gblow-qserv"   # name of keypair created
}

variable "availability-zone" {
  type    = string
  default = "nova"
}

variable "network" {
  type    = string
  default = "sandbox" # default network to be used
}

variable "security_groups" {
  type    = list(string)
  default = ["default"]  # Name of default security group
}

# Data sources
## Get Image ID
data "openstack_images_image_v2" "image" {
  name        = "ubuntu-jammy" # Name of image to be used
  most_recent = true
}

## Get flavor id
data "openstack_compute_flavor_v2" "small" {
  name = "small" # flavor to be used for jump
}

resource "openstack_networking_floatingip_v2" "admin" {
 pool = "external" 
}



# Create jump host
resource "openstack_compute_instance_v2" "admin" {
  name            = "sv-autoshutdown-admin"  #Instance name
  flavor_id       = data.openstack_compute_flavor_v2.small.id
  key_pair        = var.keypair
  availability_zone_hints = var.availability-zone
  security_groups = ["default"]
  
  block_device {
    uuid                  = data.openstack_images_image_v2.image.id
    source_type           = "image"
    volume_size           = 50
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = var.network
  }
}

resource "openstack_compute_instance_v2" "seed" {
  name            = "sv-autoshutdown-seed"  #Instance name
  flavor_id       = data.openstack_compute_flavor_v2.small.id
  key_pair        = var.keypair
  availability_zone_hints = var.availability-zone
  security_groups = ["default"]
  
  block_device {
    uuid                  = data.openstack_images_image_v2.image.id
    source_type           = "image"
    volume_size           = 50
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = var.network
  }
}

resource "openstack_compute_instance_v2" "hdd" {
  name            = "sv-autoshutdown-hdd"  #Instance name
  flavor_id       = data.openstack_compute_flavor_v2.small.id
  key_pair        = var.keypair
  availability_zone_hints = var.availability-zone
  security_groups = ["default"]
  
  block_device {
    uuid                  = data.openstack_images_image_v2.image.id
    source_type           = "image"
    volume_size           = 50
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = var.network
  }
}

resource "openstack_blockstorage_volume_v3" "hdd-vol1" {
  name = "hdd-vol1"
  size = 500
}
resource "openstack_blockstorage_volume_v3" "hdd-vol2" {
  name = "hdd-vol2"
  size = 500
}
resource "openstack_blockstorage_volume_v3" "hdd-vol3" {
  name = "hdd-vol3"
  size = 500
}

resource "openstack_compute_volume_attach_v2" "hdd-vol1_attach" {
  instance_id = openstack_compute_instance_v2.hdd.id
  volume_id   = openstack_blockstorage_volume_v3.hdd-vol1.id
}
resource "openstack_compute_volume_attach_v2" "hdd-vol2_attach" {
  instance_id = openstack_compute_instance_v2.hdd.id
  volume_id   = openstack_blockstorage_volume_v3.hdd-vol2.id
}
resource "openstack_compute_volume_attach_v2" "hdd-vol3_attach" {
  instance_id = openstack_compute_instance_v2.hdd.id
  volume_id   = openstack_blockstorage_volume_v3.hdd-vol3.id
}

resource "openstack_compute_floatingip_associate_v2" "admin" {
  floating_ip = openstack_networking_floatingip_v2.admin.address
  instance_id = openstack_compute_instance_v2.admin.id
}

# Output VM IP Address

# Output admin host IP Address
output "adminserverip" {
  value = openstack_compute_instance_v2.admin.access_ip_v4
}

resource "local_file" "ansible_hosts" {
  content = templatefile("./ansible_hosts.tftpl",
  {
    admin = openstack_compute_instance_v2.admin.name,
    seed = openstack_compute_instance_v2.seed.name,
    hdd = openstack_compute_instance_v2.hdd.name
  })
  filename = "./ansible/ansible_hosts"
}

resource "local_file" "ssh_config" {
  content = templatefile("./ssh_config.tftpl", 
  {
    admin = openstack_compute_instance_v2.admin.name, 
    admin = openstack_networking_floatingip_v2.admin.address, 
    key_name = var.keypair, 
    nodes = concat(openstack_compute_instance_v2.seed.*.name,openstack_compute_instance_v2.hdd.*.name)
    node_ips = concat(openstack_compute_instance_v2.seed.*.access_ip_v4,openstack_compute_instance_v2.hdd.*.access_ip_v4)
  })
  filename = "sas_config"
}
